package com.chargestations.app.ui.fragment.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chargestations.app.arch.api.ApiState
import com.chargestations.app.arch.repos.ChargingStationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: ChargingStationRepository) :
    ViewModel() {

    private val _userData: MutableStateFlow<ApiState> = MutableStateFlow(ApiState.EMPTY)
    val userData: StateFlow<ApiState> = _userData


    fun getChargingStations(
    ) {
        viewModelScope.launch {
            _userData.value = ApiState.LOADING
            repository.getChargingStations()
                .catch { e ->
                    _userData.value = ApiState.ERROR(e)
                }.collect { res ->
                    _userData.value = ApiState.SUCCESS(res)
                }
        }
    }


}