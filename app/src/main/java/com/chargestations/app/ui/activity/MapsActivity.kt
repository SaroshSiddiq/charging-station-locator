package com.chargestations.app.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import androidx.navigation.ui.NavigationUI
import com.chargestations.app.R
import com.chargestations.app.databinding.ActivityMapsBinding
import com.chargestations.app.ui.base.BaseActivity
import com.chargestations.app.utils.hideActionBar

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapsActivity : BaseActivity() {

    lateinit var binding: ActivityMapsBinding

    lateinit var navController: NavController

    override fun initViewBinding() {
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }


    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        this@MapsActivity.hideActionBar()
        super.onCreate(savedInstanceState)

        navController = Navigation.findNavController(this, R.id.nav_host_fragment)
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun subscribeCallbackFromSuccessStatus() {
        getResponse().observe(this, Observer {
            renderSuccessResponse(it)
        })
    }

    private fun renderSuccessResponse(response: Any?) {
        when (response) {

        }
    }


    fun navigate(resId: Int) {
        navController.navigate(resId)
    }

    fun navigate(directions: NavDirections?) {
        navController.navigate(directions!!)
    }


    override fun onSupportNavigateUp(): Boolean {
        navController.navigateUp()
        return super.onSupportNavigateUp()
    }

}