package com.chargestations.app.ui.fragment.home.response

data class ChargingStation(
    val id: Int,
    val identity: String,
    val latitude: Double,
    val longitude: Double,
    val name: String,
    val type: String
)