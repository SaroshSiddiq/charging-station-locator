package com.chargestations.app.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.chargestations.app.R
import com.chargestations.app.utils.hideActionBar
import com.chargestations.app.utils.navigateToAndfinshCurrent
import java.util.*

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        this@SplashActivity.hideActionBar()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Timer().schedule(object : TimerTask() {
            override fun run() {
                navigateToAndfinshCurrent(this@SplashActivity, MapsActivity::class.java)
            }
        }, 3000)
    }
}