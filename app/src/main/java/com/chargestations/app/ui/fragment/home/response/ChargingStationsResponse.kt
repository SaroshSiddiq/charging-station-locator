package com.chargestations.app.ui.fragment.home.response

data class ChargingStationsResponse(
    val cacheVersion: String,
    val pois: List<ChargingStation>
)