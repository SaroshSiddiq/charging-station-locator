package com.chargestations.app.ui.fragment.home

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.chargestations.app.R
import com.chargestations.app.databinding.FragmentMapsBinding
import com.chargestations.app.ui.base.BaseFragment
import com.chargestations.app.ui.fragment.home.response.ChargingStationsResponse
import com.chargestations.app.utils.gone
import com.chargestations.app.utils.visible
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_maps.view.*
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class MapsFragment : BaseFragment(), GoogleMap.OnMarkerClickListener, GoogleMap.OnMapClickListener {

    // view binding
    lateinit var binding: FragmentMapsBinding

    val viewModel by activityViewModels<HomeViewModel>()

    lateinit var mapFragment: SupportMapFragment

    private val callback = OnMapReadyCallback { googleMap ->
        val antalyaCity = LatLng(36.8980543, 30.6480645)
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(antalyaCity, 10f))

        getChargingStations()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapsBinding.inflate(inflater, container, false)

        subscribeCallbackFromSuccessStatus()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(callback)
    }


    /*  fetch tickets from server
    *... using viewmodel
    * */
    private fun getChargingStations() {
        lifecycleScope.launchWhenCreated {
            viewModel.getChargingStations()
        }
    }


    /** start listening callbacks
     * .. through livedata
     */
    private fun subscribeCallbackFromSuccessStatus() {
        getResponse().observe(requireActivity(), Observer {
            renderSuccessResponse(it)
        })

        lifecycleScope.launchWhenCreated {
            viewModel.userData.collect {
                consumeApiResponse(it)
            }
        }
    }

    /** fun renders the response coming from the
     * ... server and parse it into the respective
     * ... object
     * */
    private fun renderSuccessResponse(response: Any?) {
        when (response) {
            is ChargingStationsResponse -> {
                updateData(response)
            }
        }
    }

    private fun updateData(response: ChargingStationsResponse) {
        val bm =
            drawableToBitmap(ContextCompat.getDrawable(requireContext(), R.drawable.map_marker)!!)
        for (cor in response.pois) {
            mapFragment.getMapAsync {
                it.setOnMarkerClickListener(this)
                it.setOnMapClickListener(this)
                it.addMarker(
                    MarkerOptions()
                        .snippet("${cor.id}")
                        .position(LatLng(cor.latitude, cor.longitude))
                        .title(cor.name)
                        .icon(BitmapDescriptorFactory.fromBitmap(bm))


                )
            }

        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        binding.locationDetail.visible()
        binding.locationDetail.layout_controls.txt_title.text = marker.title
        binding.locationDetail.layout_controls.btn_direction.setOnClickListener {
            mapNavigation(marker)

        }

        return false
    }

    private fun mapNavigation(marker: Marker) {
        val navigationIntentUri =
            Uri.parse("google.navigation:q=" + marker.position.latitude + "," + marker.position.longitude)
        val mapIntent = Intent(Intent.ACTION_VIEW, navigationIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        startActivity(mapIntent)
    }

    override fun onMapClick(latlng: LatLng) {
        binding.locationDetail.gone()
    }

    fun drawableToBitmap(drawable: Drawable): Bitmap? {
        var width = drawable.intrinsicWidth
        width = if (width > 0) width else 1
        var height = drawable.intrinsicHeight
        height = if (height > 0) height else 1
        val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }
}