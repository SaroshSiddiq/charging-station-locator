package com.chargestations.app.arch.api

import com.chargestations.app.ui.fragment.home.response.ChargingStationsResponse
import retrofit2.http.*

interface Api {
    companion object {
        // const val BASE_URL = "http://127.0.0.1:8000/"
        const val BASE_URL = "https://secure3.vektorteknoloji.com/carshare/public/poi/"

        const val GET_CHARGING_STATIONS = "charging-stations"

    }

    @GET(GET_CHARGING_STATIONS)
    suspend fun getChargingStations(
    ): ChargingStationsResponse


}