package com.chargestations.app.arch.repos

import com.chargestations.app.arch.api.Api
import com.chargestations.app.ui.fragment.home.response.ChargingStationsResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject


class ChargingStationRepository @Inject constructor(val apiHelper: Api) {

    fun getChargingStations(): Flow<ChargingStationsResponse> = flow {
        val response = apiHelper.getChargingStations()
        emit(response)
    }.flowOn(Dispatchers.IO)


}